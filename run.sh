function build {
    for i in $(ls -A "home"); do
        local home="$HOME/$i"
        local dot="$PWD/home/$i"
        local backup="$home.backup"

        [ -f $home ] && [ ! -f $backup ] && mv $home $backup \
            || echo "Error: backup file for $i already exists"

        ln -s $dot $home
    done
}

if [ ! -z $1 ]; then
    $1
else
    echo "available commands"
    echo "  build: create the simbolic links"
fi
