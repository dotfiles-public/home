source "$HOME/.local/dotfiles/lib/export.sh"
source "$HOME/.local/dotfiles/lib/alias.sh"

# exclusive aliasses for zsh
alias ll="exa -lam --no-user --time-style long-iso --group-directories-first -s extension --icons"
alias l="exa -lm --no-user --time-style long-iso --group-directories-first -s extension --icons"

# ---------------------------
# zsh personal customizations

# Tab completion for zshell
autoload -U compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit
_comp_options+=(globdots) # Include hidden files.

# Use vi keys to navigate in completions
bindkey -e
bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'l' vi-forward-char
bindkey -M menuselect 'j' vi-down-line-or-history

# Vi mode in Zsh prompt
bindkey -v
bindkey -s '^p' '\e'
export KEYTIMEOUT=1

autoload edit-command-line
zle -N edit-command-line
bindkey '^n' edit-command-line

# ---------------------------
# powerlevel10k configuration

# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

# Use powerline
USE_POWERLINE="true"

source "/usr/share/zsh-theme-powerlevel10k/powerlevel10k.zsh-theme"

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[ ! -f "$HOME/.local/dotfiles/lib/p10k.zsh" ] || source "$HOME/.local/dotfiles/lib/p10k.zsh"

# --------------------------
# zsh plugins and extensions

# Manual installed zsh plugins
source "$HOME/.zsh/zsh-autosuggestions/zsh-autosuggestions.zsh"
source "$HOME/.zsh/fast-syntaxe-highlighting/fast-syntax-highlighting.plugin.zsh"

# Autojump plugin configuration
# [[ -s $HOME/.autojump/etc/profile.d/autojump.sh ]] && source $HOME/.autojump/etc/profile.d/autojump.sh
# autoload -U compinit && compinit -u
# setopt autocd
