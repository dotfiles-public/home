# exports to config some apps
export PNPM_HOME="$HOME/.local/share/pnpm"          # PNPM binary directory
export PERSONAL_SCRIPTS="$HOME/.dotfiles/stuff/bin" # Local scripts
export BAT_THEME="Nord"                             # Bat theme
export EDITOR="vim"                                 # Default editor
export PECO="$HOME/.config/peco/config.json"        # Peco settings

# "gambiarra" to use my new scripts repository
export PERSONAL_SCRIPTS="$PERSONAL_SCRIPTS:$HOME/.local/scripts/bin"

# system and user paths export
PATH="/bin:/usr/bin:/usr/local/bin:/usr/local/sbin"                     # system default bin
PATH="$PATH:/usr/lib/jvm/default/bin"                                   # java bin
PATH="$PATH:$HOME/.local/share/gem/ruby/3.0.0/bin"                      # ruby binaries
PATH="$PATH:/usr/bin/vendor_perl:/usr/bin/site_perl:/usr/bin/core_perl" # perl libs
PATH="$PATH:$HOME/bin:$HOME/.local/bin"                                 # user default bin paths
PATH="$PATH:$HOME/.cargo/bin"                                           # rust path
PATH="$PATH:$PERSONAL_SCRIPTS"                                          # personal scripts
PATH="$PATH:$HOME/.local/AppImages/bin"                                 # appImages symbolic links
PATH="$PATH:/usr/lib/jvm/java-8-openjdk/jre/bin"                        # java binaries (?)
PATH="$PATH:$HOME/.fly/bin"                                             # binary to run the Fly.io command line utility
PATH="$PATH:$HOME/.yarn/bin"                                            # yarn global packages binaries
PATH="$PATH:$PNPM_HOME"                                                 # PNPM path
export PATH
