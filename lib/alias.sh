# gotta go fast
alias r="rm -rf"
alias g="git"
alias n="nvim"
alias v="vim"
alias e="exit"
alias f="ranger"
alias t="tmux"
alias ct="cd $(mktemp -d)"
alias md="mkdir -vp"
alias cp="cp -r"
alias ..="cd .."

# color ls and ripgrep
alias ll="ls -lhA --color=auto"
alias l="ls -h --color=auto"

# "frescurites e perfumaria"
alias neofetch="neofetch --ascii $HOME/.config/neofetch/ascii"

# "frescurites e perfumaria" part two
alias fzf="fzf --border sharp --margin 10% --padding 5% --info inline --prompt 'SEARCH: ' --pointer '**' --ansi --color 'bg+:-1,pointer:green,fg+:green,hl:yellow,border:gray'"
alias tree="tree --dirsfirst -F -a -I .git -I node_modules -I target -I .mypy_cache -I __pycache__"
alias grep="grep --colour=auto"

# execution permition
alias exe="chmod +x"

# python related
alias pip="python -m pip"
alias pyserver="python -m http.server 8080"
alias pt="poetry"
alias ptr="poetry run"

# arduino-cli related
alias ardCompile="arduino-cli compile -u -b arduino:avr:uno -p /dev/ttyUSB0"
alias ardList="arduino-cli board list"
alias ardInstall="arduino-cli lib install"
alias ardUninstall="arduino-cli lib uninstall"
alias ardNew="arduino-cli sketch new"

# my name convetion
alias NC="date '+%d%m%Y-%H%M%S'"

# projects shortcuts function
function goto {
    local PROJECTS="$HOME/External/Projects"

    case ${1} in
        gh) cd "$PROJECTS/github.com" ;;
        gl) cd "$PROJECTS/gitlab.com" ;;
        cb) cd "$PROJECTS/codeberg.org" ;;
        i)  cd "$PROJECTS/ilustrations" ;;
        t)  cd "$PROJECTS/trash" ;;
    esac
}
